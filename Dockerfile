FROM python:3.5.2

WORKDIR /server
ADD requirements.txt /server/requirements.txt
RUN pip install -r requirements.txt

CMD jupyter notebook --ip=0.0.0.0 --port=8888 --allow-root --NotebookApp.token=''
