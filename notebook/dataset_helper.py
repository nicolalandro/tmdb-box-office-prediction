import pandas as pd
import ast

def text_to_dict(df, dict_columns):
    for column in dict_columns:
        df[column] = df[column].apply(lambda x: {} if pd.isna(x) else ast.literal_eval(x) )
    return df

def read_dataset():
    train = pd.read_csv('../input/train.csv')
    dict_columns = ['belongs_to_collection', 'genres', 'production_companies',
                    'production_countries', 'spoken_languages', 'Keywords', 'cast', 'crew']
        
    train = text_to_dict(train, dict_columns)
    return train
